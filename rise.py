#!/usr/bin/env python
"""This script takes stadard RAMSES movie outputs
   and creates movie with ffmpeg"""
import sys
import os
from argparse import ArgumentParser
from matplotlib import pyplot as plt
import matplotlib.patches as mpatches
from matplotlib.collections import PatchCollection
import numpy as np
from pyramid import load_namelist
from pyramid import load_units, load_sink, load_map, load_times
from ram import MovieFrame, tile_add_lenbar, tile_add_label, Camera, get_sink_pos
from ram import tile_add_colorbar

def make_image(fig, i, index, config, nml, all_sinks, all_times, units):
    """Making image"""

    if not os.path.exists("{}/movie1/info_{:05d}.txt".format(config.dir, index)):
        return

    boxlen = nml['amr_params']['boxlen']
    nx = nml['movie_params']['nw_frame']
    ny = nml['movie_params']['nh_frame']

    # need to load units here due to cosmo
    if nml['run_params']['cosmo']:
        units = load_units(config.dir, index, 1)

    if not nml['run_params']['cosmo']:
        current_aexp = 1.
        current_time = all_times[(index-int(config.fmin))/config.step]
    else:
        current_aexp = all_times[(index-int(config.fmin))/config.step]
        current_time = np.nan


    proj = config.proj-1
    axis = nml['movie_params']['proj_axis'][proj]

    # load data
    data = MovieFrame("{}/movie{:1d}/{}_{:05d}.map"
                      .format(config.dir,
                              config.proj,
                              config.kind,
                              index))
    rollby = [0, 0]
    if config.roll_axis == 'x':
        rollby[0] = nx/2
        data.data = np.roll(data.data, rollby[0], axis=1)
    if config.roll_axis == 'y':
        rollby[1] = ny/2
        data.data = np.roll(data.data, rollby[1], axis=0)

    data.scaledata(units)
    data.setlimits(float(config.min), float(config.max))

    if config.logscale:
        data.setlogscale()

    ############
    # Plotting #
    ############
    # axis = fig.add_subplot(1, len(config.image_indexes.split(',')), i+1)
    axis = fig.add_subplot(config.rise_geometry[0], config.rise_geometry[1], i+1)
    axis.axis([0, data.nx, 0, data.ny])
    fig.add_axes(axis)

    # show image
    image = axis.imshow(data.data, interpolation='none', cmap=config.colormap,
                        vmin=data.plotmin, vmax=data.plotmax, aspect='auto')
    # remove ticks
    axis.tick_params(bottom=False, top=False, left=False, right=False)
    axis.tick_params(labelbottom=False, labeltop=False,
                     labelleft=False, labelright=False)

    # add length bar
    patches = []
    if len(config.bar) > 0:
        plt_bar = tile_add_lenbar(axis, config.bar, config.labelcolor, config.fontsize, data, units)
        patches.append(plt_bar)
    axis.add_collection(PatchCollection(patches, facecolor=config.labelcolor))
    # add label
    if config.label != "":
        tile_add_label(axis, config.label, config.labelcolor, config.fontsize)

    # add sink
    sinks = all_sinks[(index-int(config.fmin))/config.step]
    if config.sink_flags and nml['run_params']['sink'] and sinks is not None:
        if not nml['run_params']['cosmo']:
            camera_now = Camera(nml, proj, current_time)
            time_now = current_time
        else:
            camera_now = Camera(nml, proj, current_aexp)
            time_now = current_aexp

        # r_sink = 1/2**(level_max) * ir_cloud(default=4) * boxlen;
        # then convert into pts
        r_sink = (0.5**(nml['amr_params']['levelmax'])*4.*boxlen*data.nx/data.fdw*1.5)
        if config.true_sink:
            area_sink = r_sink*r_sink
        else:
            area_sink = 100

        for sink in sinks:
            xsink, ysink, zsink = get_sink_pos(sink, time_now, camera_now, nml, proj)

            # Plotting sink if fits in depth
            if abs(zsink) < data.fdd/boxlen/2.:
                axis.scatter(xsink/(data.fdw/boxlen/2.)*data.nx/2 +
                             data.nx/2-rollby[0],
                             ysink/(data.fdh/boxlen/2.)*data.ny/2 +
                             data.ny/2-rollby[1],
                             marker='o',
                             facecolor='none',
                             edgecolor=config.labelcolor,
                             s=area_sink,  # s takes area in pts
                             lw=2)

    # add streamlines from magnetic field lines
    if config.kind == 'pmag' and config.streamlines is True:
        if axis == 'x':
            dat_u = np.array(load_map(config, -1, index, 'byl')).reshape(ny, nx)
            dat_v = np.array(load_map(config, -1, index, 'bzl')).reshape(ny, nx)
        elif axis == 'y':
            dat_u = np.array(load_map(config, -1, index, 'bxl')).reshape(ny, nx)
            dat_v = np.array(load_map(config, -1, index, 'bzl')).reshape(ny, nx)
        elif axis == 'z':
            dat_u = np.array(load_map(config, -1, index, 'bxl')).reshape(ny, nx)
            dat_v = np.array(load_map(config, -1, index, 'byl')).reshape(ny, nx)
        pmagx = np.linspace(0, nx, num=nx, endpoint=False)
        pmagy = np.linspace(0, ny, num=ny, endpoint=False)
        axis.streamplot(pmagx, pmagy, dat_u, dat_v, density=0.25,
                        color=config.labelcolor, linewidth=1.0)

    # add timer
    if config.timer:
        if nml['run_params']['cosmo']:
            axis.text(0.05, 0.95, 'z={z:.2f}'.format(z=abs(1./current_aexp-1)),
                      verticalalignment='bottom',
                      horizontalalignment='left',
                      transform=axis.transAxes,
                      color=config.labelcolor,
                      fontsize=config.fontsize)
        else:
            current_time *= units.t/86400/365.25  # time in years
            time_print_set = '%.{}f %s'.format(1)
            if current_time >= 1e3 and current_time < 1e6:
                scale_t = 1e3
                t_unit = 'kyr'
            elif current_time > 1e6 and current_time < 1e9:
                scale_t = 1e6
                t_unit = 'Myr'
            elif current_time > 1e9:
                scale_t = 1e9
                t_unit = 'Gyr'
                time_print_set = '%.{}f %s'.format(2)
            else:
                scale_t = 1.
                t_unit = 'yr'

            axis.text(0.05, 0.95, time_print_set % (current_time/scale_t, t_unit),
                      verticalalignment='bottom',
                      horizontalalignment='left',
                      transform=axis.transAxes,
                      color=config.labelcolor,
                      fontsize=config.fontsize)

    # add colorbar
    if config.colorbar:
        tile_add_colorbar(fig, image, i, data.plotmin, data.plotmax,
                          config.rise_geometry, config.cbar_width,
                          config.labelcolor, config.fontsize)


    return

def main():
    """Main body - parses arguments & create images"""
    __version__ = "0.1.2"
    advert = """\
            _____ _____ _____ _____ 
           | __  |     |   __|   __|
           |    -|-   -|__   |   __|
           |__|__|_____|_____|_____|
                                      
            RAMSES Image SEquencer
      v {version} - 2018/01/11 - P. Biernacki
    """.format(version=__version__)
    print advert
    # Check if running python2
    if sys.version_info.major != 2:
        print "This script works only in python2, sorry!"
        sys.exit()

    # Parse command line arguments
    parser = ArgumentParser(description="Script to create set of panels from RAMSES movies")
    parser.add_argument("-c", "--config", dest="config", metavar="VALUE",
                        type=str, help="config file  [%(default)s]",
                        default="./config.ini")
    parser.add_argument("-d", "--debug", dest="debug", action='store_true',
                        help="debug mode [%(default)r]",
                        default=False)
    parser.add_argument("-o", "--output", dest="outfile", metavar="FILE",
                        type=str, help="output image file [<map_file>.png]",
                        default=None)

    for k in xrange(len(sys.argv)):
        if sys.argv[k] in ["-c", "--config"]:
            config_file = sys.argv[k+1]
            break
        else:
            config_file = "./config.ini"

    from rise_load_settings import load_settings
    config = load_settings(parser, config_file)

    # load basic info once, instead of at each loop
    nml = load_namelist(config.namelist, config.dir)

    # load units if not cosmo
    if not nml['run_params']['cosmo']:
        units = load_units(config.dir, config.fmax, 1)
    else:
        units = None

    image_indexes = config.image_indexes.split(',')

    # load sinks
    all_sinks = []
    all_times = []

    for index in xrange(config.fmin, config.fmax+1, config.step):
        if config.sink_flags:
            sinks_i = load_sink(config.dir, index, nml['amr_params']['boxlen'])
            all_sinks.append(sinks_i)
        time_i = load_times(config.dir, index, nml['run_params']['cosmo'])
        all_times.append(time_i)

    # creating images
    nx = int(nml['MOVIE_PARAMS']['nw_frame'])
    ny = int(nml['MOVIE_PARAMS']['nh_frame'])
    fig = plt.figure(frameon=False)
    fig.set_size_inches(nx/100.*config.rise_geometry[1],
                        ny/100.*config.rise_geometry[0])

    for index in xrange(len(image_indexes)):
        make_image(fig, index, int(image_indexes[index]), config, nml,
                   all_sinks, all_times, units)


    # corrects window extent
    plt.subplots_adjust(left=0., bottom=0.,
                        right=1.+1.0/nx, top=1.+1.0/ny,
                        wspace=0., hspace=0.)
    if config.rise_fname != "":
        filename = "{dir}/{fname}".format(dir=config.dir, fname=config.rise_fname)

    plt.savefig(filename, dpi=100)
    plt.close(fig)

if __name__ == '__main__':
    main()
